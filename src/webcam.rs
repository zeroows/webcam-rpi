extern crate rscam;
extern crate ws;

use x264_encoder::X264Encoder;
use ws::{Message, Sender};
use rscam::{Camera};
use std::thread;
use std::collections::HashMap;
use std::sync::mpsc::{channel, TryRecvError};
use std::sync::{Arc, Mutex};
use std::time::{Instant, Duration};

pub struct Webcam{
    senders:Arc<Mutex<HashMap<String, Arc<Sender>>>>,
    device:String,
    fps:u32,
    width:i32,
    height:i32,
    preset:String,
    running: Arc<Mutex<bool>>
}

impl Drop for Webcam {
    fn drop(&mut self) {
        println!("Webcam Dropping!");
    }
}

impl Webcam{
    pub fn new(device:String, fps:u32, width:i32, height:i32)->Webcam{
        Webcam{
            senders: Arc::new(Mutex::new(HashMap::new())),
            fps: fps,
            width: width,
            height: height,
            device: device,
            preset: "veryfast".to_string(),
            running: Arc::new(Mutex::new(false)),
        }
    }

    pub fn fps(&mut self, fps:u32){
	    self.fps = fps;
    }

    pub fn width(&mut self, width:i32){
	    self.width = width;
    }

    pub fn height(&mut self, height:i32){
	    self.height = height;
    }

    pub fn preset(&mut self, preset:String){
        self.preset = preset;
    }

    //加入视频
    pub fn join_webcam(&mut self, key:String, receiver:Arc<Sender>){
        let mut senders = self.senders.lock().unwrap();
        if senders.contains_key(&key){
            return;
        }

        let mut self_running = self.running.lock().unwrap();
        senders.insert(key.clone(), receiver);
        if *self_running{
            return;
        }

        *self_running = true;
	    println!("Start Webcam.");

        let (data_sender, data_receiver) = channel();
        let (msg_sender, msg_receiver) = channel();
        let fps = self.fps;
	    let (width, height) = (self.width, self.height);
        let preset = self.preset.clone();
        let device = self.device.clone();

        thread::spawn(move|| {
            let encoder = X264Encoder::new(fps, preset, "zerolatency".to_string(), width, height);
            if encoder.is_err(){
                println!("X264Encoder initialization failed.");
                let err = encoder.err();
                data_sender.send(Err(format!("{:?}", err.clone()))).unwrap();
                return Err(format!("{:?}", err));
            }

            let mut encoder = encoder.unwrap();

            let open_result = Camera::new(&device);
            if open_result.is_ok(){
                println!("The camera is turned on.");
                let mut camera = open_result.unwrap();
                let start_result =  camera.start(&rscam::Config {
                    interval: (1, 30),
                    resolution: (width as u32, height as u32),
                    format: b"YU12",
                    ..Default::default()
                });
                if start_result.is_err(){
                    println!("Camera failed to start");
                    println!("Webcam photo thread ends.{:?}", start_result.err());
                }else{
                    println!("Camera started.");
                    //Check whether the request is over
                    loop{
                        let msg_recv = msg_receiver.try_recv();
                        if msg_recv.is_ok(){
                            println!("Webcam photo thread ends.{}", msg_recv.unwrap());
                        }else if msg_recv.err().unwrap() == TryRecvError::Disconnected{
                            println!("Webcam photo thread ends.{:?}", msg_recv.err());
                            break;
                        }
                        let start_time = Instant::now();
                        let frame_resut = camera.capture();
                        if frame_resut.is_err(){
                            println!("Failed to take a picture");
                            println!("Webcam photo thread ends.{:?}", frame_resut.err());
                            break;
                        }
                        let frame = frame_resut.unwrap();
                        
                        let encode_result = encoder.encode(Vec::from(&frame[..]));
                        
                        if encode_result.is_err(){
                            data_sender.send(Err(format!("{:?}", encode_result.err()))).unwrap_or_default();
                            break;
                        }

                        let d = start_time.elapsed();

                        //Send image data
                        if let Err(err) = data_sender.send(Ok(encode_result.unwrap())){
                            println!("Webcam photo thread ends.{:?}", err);
                            break;
                        }

                        //Calculate the time spent and delay
                        let elapsed_ms = (start_time.elapsed().subsec_nanos()/1_000_000) as u64;
                        //println!("time:{}", elapsed_ms);
                        if elapsed_ms<1000/fps as u64{
                            thread::sleep(Duration::from_millis(1000/fps as u64-elapsed_ms));
                        }
                    }
                    //Close Camera
                    let _ = camera.stop().unwrap_or_default();
                }
                Ok(())
            }else{
                println!("Camera failed to open.");
                data_sender.send(Err(String::from("Camera failed to open!"))).unwrap();
                Err(format!("{:?}", open_result.err()))
            }
        });

        //Turn on receive thread
        let senders_clone = self.senders.clone();
        let running_clone = self.running.clone();
        thread::spawn(move|| {
            loop{
                let recv = data_receiver.recv();
                if recv.is_err() {
                    println!("{:?}", recv.err());
                    break;
                }else{
                    let data_result = recv.unwrap();
                    if data_result.is_err(){
                        println!("{:?}", data_result.err());
                        break;
                    }
                    let senders = senders_clone.lock().unwrap();
                    if senders.len() == 0{
                        println!("Webcam.senders is empty.");
                        msg_sender.send("End.").unwrap_or_default();
                        break;
                    }
                    let frame_data = data_result.unwrap();
                    for (_key, sender) in senders.iter(){
                        //Send data to Sender
                        let _ = sender.send(Message::binary(frame_data.as_slice())).unwrap_or_default();
                    }
                }
            }
            *running_clone.lock().unwrap() = false;
            senders_clone.lock().unwrap().clear();
            println!("Webcam Ended.");
        });
        println!("Webcam start.");
    }

    //Exit video
    pub fn quit_webcam(&mut self, key:&String){
        let mut senders = self.senders.lock().unwrap();
        senders.remove(key);
    }
}
